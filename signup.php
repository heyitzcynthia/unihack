<?php
   include("config.php");
   session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $name = $_POST["name"];
	$address = $_POST["address"];
	$phone = $_POST["phone"];
	$username = $_POST["username"];
	$password = $_POST["password"];


	$statement = mysqli_prepare($con, "INSERT INTO user(name,address,phone,username,password) VALUES (?,?,?,?)");
	mysqli_stmt_bind_param($statement, "sssss", $name, $address, $phone, $username, $password);
	mysqli_stmt_execute($statement);
	
	$response = array();
	$response["success"] = true;

    header("location: home.php");
      
   }
?>

<!DOCTYPE html>
<html>
   <head>
	<title>Sign Up</title>
	<link href="dynamicPage.css" rel="stylesheet" type="text/css">
	<link href="puppy.css" rel="stylesheet" type="text/css">
</head>
<body>


	<header>
    	<nav>
    		<h1>Walk a Dog</h1>
    		<ul>
    			<li><a href="about.html">About Us</a></li>
    			<li><a href="#">Location</a></li>
    			<li><a href="#">Become a Walker</a></li>
    			<li><a href="#">Help</a></li>
    			<li><a href="#">Sign Up</a></li>
    			<li><a href="home.php">Login</a></li>
    		</ul>
    	</nav>

    </header>
	
	
	<div align = "center">
				
			<br><br>
			<img src="logo.jpg" alt="dog image" height="200" width="200">

				<div id="dogs">
					<br><br>
					<h1> Sign up to Walk a Dog Today</h1>
					<form action="" method="post">
					<input type="text" placeholder="Name" name="name" required="required">
					<br>
					<input type="text" placeholder="Address" name="address" required="required"><br>
					<input type="text" placeholder="Phone Number" name="phone" required="required">
					<br><input type="text" placeholder="Username" name="username"id="username" required="required"><br>
					<input type="password" placeholder="Password" name="password" id="password" required="required"><br><br>
					<input type="button" value="Submit">
					</form>
				</div>
			

      </div>
	
</body>
</html>