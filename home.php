<?php
   include("config.php");
   session_start();
   
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($db,$_POST['username']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']); 
      
      $sql = "SELECT * FROM users WHERE username = '$myusername' AND password = '$mypassword'";

      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $active = $row['active'];
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
		
      if($count == 1) {
         session_register("myusername");
         $_SESSION['login_user'] = $myusername;
         
         header("location: welcome.html");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
 
<!DOCTYPE html>
<html>
    <head>
	<title>Walk a Dog</title>
	
	<link href="puppy.css" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
    	<nav>
    		<h1>Walk a Dog</h1>
    		<ul>
    			<li><a href="about.html">About Us</a></li>
    			<li><a href="#">Location</a></li>
    			<li><a href="#">Become a Walker</a></li>
    			<li><a href="#">Help</a></li>
    			<li><a href="signup.php">Sign Up</a></li>
    			<li><a href="#" disabled="true">Login</a></li>
    		</ul>
    	</nav>

    </header>
	
	<div align = "center">
				
			<br><br>
			<img src="logo.jpg" alt="dog image" height="200" width="200">

				<div id="dogs">
					<br><br>
					<h1> Welcome to Walk a Dog</h1>
					<form action="" method="post">
					<br><input type="text" name="username" placeholder="Username" id="username" required="required"><br><br>
					<input type="password" name="password" placeholder="Password" id="password" required="required"><br><br>
					<a href="about.html"><button id="login_btn">Login</button></a><br><br>
					</form>
					Not yet a member? <a href="signup.html">Register Now</a>
					<br><br>
					<?php echo"$error"?>
				</div>
			

      </div>

</body>
</html>